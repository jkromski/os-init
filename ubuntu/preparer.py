#!/bin/python3
import os
from enum import Enum
import yaml


class PrepType(Enum):
    APT = "apt"
    AUTOSTART = "autostart"


class Item:
    def __init__(self, name):
        self.name = name

    def run(self):
        raise EnvironmentError("no implementation")


class Apt(Item):
    def __init__(self, name):
        super(Apt, self).__init__(name)

    def run(self):
        return os.system("sudo apt install --assume-yes " + self.name)


class Snap(Item):
    def __init__(self, name):
        super(Snap, self).__init__(name)

    def run(self):
        return os.system("sudo snap install --classic " + self.name)


class Pip(Item):
    def __init__(self, name):
        super(Pip, self).__init__(name)

    def run(self):
        return os.system("pip install " + self.name)


class Script(Item):

    def __init__(self, name, commands: list):
        super(Script, self).__init__(name)
        self.commands = commands
    
    def run(self):
        for i in self.commands:
            os.system(i)


class Autostart(Item):

    def __init__(self, name, command, description = None):
        super(Autostart, self).__init__(name)
        self.command = command
        self.description = description

    def run(self):
        path = "~/.config/autostart/" + self.command + ".desktop"
        name = self.name if self.name else self.command
        description = self.description if self.description else self.command
        content = "[Desktop Entry]\n"
        content += "Type=Application\n"
        content += "Exec=" + self.command + "\n"
        content += "Hidden=false\n"
        content += "NoDisplay=false\n"
        content += "X-GNOME-Autostart-enabled=true\n"
        content += "Name[en_GB]=" + name + "\n"
        content += "Name=" + name + "\n"
        content += "Comment[en_GB]=" + description + "\n"
        content += "Comment=" + description + "\n"

        file = open(path, "w")
        file.write(content)
        file.close()


class UbuntuPreparer:

    def __init__(self, items_to_install):
        self.installations = items_to_install
        self.results = []

    def prepare(self):
        self.results.clear()
        for i in self.installations:
            self.results.append(
                {
                    "name": i.name,
                    "result": i.run()
                }
            )

    def print_results(self):
        for result in self.results:
            if result["result"] == 0:
                print(result["name"] + " #OK")
            else:
                print(result["name"] + " #FAILED")


if __name__ == "__main__":

    packages = []
    with open('packages.yaml') as packages_file:

        config = yaml.load(packages_file, Loader=yaml.FullLoader)
        print(config)

        for item in config:
            if type(item) == str:
                packages.append(Apt(item))
            elif 'manager' in item and item['manager'] == 'apt':
                packages.append(Apt(item['name']))
            elif 'manager' in item and item['manager'] == 'snap':
                packages.append(Snap(item['name']))
            elif 'manager' in item and item['manager'] == 'script':
                packages.append(Script(item['name'], item['commands']))
            else:
                print("Item not supported:" + item)

    print("installing packages ...\n")
    prep = UbuntuPreparer(packages)
    prep.prepare()
    prep.print_results()
